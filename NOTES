Todo:

* Verify the max-domain-name-length constant.

* Implement A-label roundtrip conversion.

* Decide whether non-U+002E dots need any special consideration.

Comments on the specification:

* The [Unicode] reference needs to be a normative reference.  In
  section 4.2.3.2 there is a MUST requirement to reject strings that
  begin with combining mark/character.  The exact definition of what a
  combining mark/character is only given in [Unicode].

  Strictly speaking, without a normative and stable definition of what
  a combining mark/character is it is not possible to implement
  IDNA2008.

* The [Unicode-UAX15] reference is not to a stable document, the link
  goes to a living document that is actively modified.

  Strictly speaking, without a normative and stable definition of the
  NFC algorithm it is not possible to implement IDNA2008.

* Section 5.5 of RFC 5891 is performed on A-labels too, causing double
  ACE encoding.

* There is no discussion of how strings are separated into labels.
  For example section 5 of RFC 5891 discuss entire strings in section
  5.1 and 5.2 and section 5.3 onwards discuss labels only.

* Re U+200C, there are no Joining Type T code points that are
  permitted in IDNA2008.

* IdnaTest.txt test vectors found two bugs in libidn2: one cut'n'paste
  bug of the U+200C rule (L vs R) and handling of zero size labels.

Example domains IDNA2003-valid but IDNA2008-lookup-invalid:

* example.∡↺⊂ (symbols)
* ¹1.example
* Ⅵvi.example
* 3002-test.ídn aka 3002-test.xn--dn-mja (U+3002 acts as a label separator)
* simplecapDídn.example aka xn--simplecapddn-1fb.example (ASCII 0x44)
* latintogreekµídn.example aka xn--latintogreekdn-cmb716i.example (U+00B5)
* xn--latinextdn-v6a6e.example aka xn--latinextdn-v6a6e.example (U+00C7)
* turkishiİídn.example aka xn--turkishiidn-wcb701e.example (U+0130)
* nonbmp𐐀ídn.example aka xn--nonbmpdn-h2a34747d.example (U+101400)
* newnorm当ídn.example aka xn--newnormdn-m5a7856x.example (U+2F874)

Example domains IDNA2008-lookup-valid but IDNA2003-invalid:

* -foo (leading hyphen)

Example domains with different ACE encodings in IDNA2003 and IDNA2008-lookup:

* ßss.example
  IDNA2003: ssss.example
  IDNA2008-lookup: xn--ss-fia.example
* sharpsßídn.example
  IDNA2003: xn--sharpsssdn-r8a.example
  IDNA2008: xn--sharpsdn-vya4l.example
